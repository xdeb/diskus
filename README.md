# diskus

A minimal, fast alternative to 'du -sh'.

This repository provides the latest Debian/Ubuntu package.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/diskus/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/diskus/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
    content as following (debian/bookworm for example):

        deb https://xdeb.gitlab.io/diskus/debian bookworm contrib

    The available source list files are:

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/diskus/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/diskus/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/diskus/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/diskus/ubuntu jammy universe

    * Ubuntu 24.04 LTS (Noble Numbat)

        deb https://xdeb.gitlab.io/diskus/ubuntu noble universe

3. Install `diskus`

        apt-get update && apt-get install diskus

## Usage (download binary)

All binaries are stored at `https://xdeb.gitlab.io/diskus/diskus_<OS>_<ARCH>.gz`.

For example, you can download x64 binary for _Debian 12_ at <https://xdeb.gitlab.io/diskus/diskus_bookworm_amd64.gz>. You can also download i386 binary for _Ubuntu 24.04_ at <https://xdeb.gitlab.io/diskus/diskus_noble_i386.gz>. Then unzip the downloaded file by `gzip -d` command. Change the file permission by `chmod` command finally.

For instance, download the binary on _Debian 12 (amd64)_, and install it, we can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/diskus/diskus_bookworm_amd64.gz | gzip -d > /usr/local/bin/diskus && chmod 755 /usr/local/bin/diskus
